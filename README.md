# API Usuarios (Prueba para Backend Java Developer)
Este proyecto consiste en la exposición de una API que permite la creación de usuarios en base a las siguientes reglas de negocio:
y pues este es un cambio para testear como funciona
## Reglas
* All messages must follow the format -> ```json {"mensaje": "mensaje de error" } ```
* Ese endpoint deberá recibir un usuario con los campos ```"nombre", "correo", "contraseña"```, más un listado de objetos ```"teléfono"```, respetando el siguiente formato:
    
    ```json
    {
        "name": "Juan Rodriguez",
        "email": "juan@rodriguez.org",
        "password": "hunter2",
        "phones": [
            {
                "number": "1234567",
                "citycode": "1",
                "countrycode": "57"
            }
        ]
    } 
    ```
* Responder el código de status HTTP adecuado
* En caso de éxito, retorne el usuario y los siguientes campos:
    - id: id del usuario (puede ser lo que se genera por el banco de datos, pero sería más deseable un UUID)
    - created: fecha de creación del usuario
    - modified: fecha de la última actualización de usuario
    - last_login: del último ingreso (en caso de nuevo usuario, va a coincidir con la fecha de creación)
    - token: token de acceso de la API (puede ser UUID o JWT)
    - isactive: Indica si el usuario sigue habilitado dentro del sistema.
*  Si caso el correo conste en la base de datos, deberá retornar un error "El correo ya se encuentra registrado"
* El correo debe seguir una expresión regular para validar que formato sea el correcto. (aaaaaaa@dominio.cl)
* La clave debe seguir una expresión regular para validar que formato sea el correcto. (El
valor de la expresión regular debe ser configurable)
* El token deberá ser persistido junto con el usuario

## Technologies
* Spring Boot 3.0
* Spring Security
* Spring Web
* Spring JPA
* JSON Web Tokens (JWT)
* BCrypt
* Maven
 
## Cómo ejecutar el projecto
Para comenzar con este proyecto, deberá tener instalado lo siguiente en su máquina local:

* JDK 11+
* Maven 3+

Para compilar y ejecutar el proyecto, siga estos pasos:

* Clonar el repositorio: `git clone https://gitlab.com/oms.omar/api-usuarios.git`
* Construir los paquetes: mvn clean install
* Construir el docker project: docker build -t api-usuarios.jar .
* Ejecutar el container: docker run --name api-usuarios -p 8090:8090 api-usuarios.jar

-> La aplicación estará disponible en http://localhost:8090
-> El acceso a la base de datos estará disponible en http://localhost:8090/h2-console

## Diagrama de Solución

![Diagrama de Solución](diagrama.jpg)
