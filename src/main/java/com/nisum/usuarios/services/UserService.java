package com.nisum.usuarios.services;

import com.nisum.usuarios.dto.*;
import com.nisum.usuarios.model.*;
import com.nisum.usuarios.exceptions.EmailDuplicatedException;
import com.nisum.usuarios.repositories.TokenRepository;
import com.nisum.usuarios.repositories.UserRepository;
import com.nisum.usuarios.security.JwtService;
import com.nisum.usuarios.util.PasswordValidation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserService implements IUserService {

    private final UserRepository userRepository;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    private final PasswordValidation passwordValidation;
    private final UserMapper userMapper;
    private final TokenRepository tokenRepository;

    @Override
    @Transactional
    public CreateUserResponse register(UserRegisterRequest request) {
        userRepository.findByEmail(request.getEmail())
                .ifPresent(e -> { throw new EmailDuplicatedException("El correo ya se encuentra registrado.", HttpStatus.CONFLICT.value()); });
        validatePassword(request.getPassword());
        User savedUser = userMapper.userDTOtoUser(request);
        String jwtToken = jwtService.generateToken(savedUser);
        saveUserToken(savedUser, jwtToken);
        return userMapper.userToUserResponseDTO(savedUser, jwtToken);
    }

    @Override
    public List<UserDTO> getAllUsers() {
        return userMapper.usersToUsersDTO(userRepository.findAll());
    }

    @Override
    public void logout(UserDTO userDTO) {
        revokeAllUserTokens(userRepository.findById(userDTO.getId()).orElseThrow());
    }

    public AuthenticationResponse authenticate(AutheticationRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(),
                        request.getPassword()
                )
        );
        User user = userRepository.findByEmail(request.getEmail())
                .orElseThrow(() -> new UsernameNotFoundException("No se encontró usuario con el username: " + request.getEmail()));
        return AuthenticationResponse
                .builder()
                .token(jwtService.generateToken(user))
                .build();
    }

    private void validatePassword(String password) {
        passwordValidation.isValidPassword(password);
    }

    private void saveUserToken(User user, String jwtToken) {
        Token token = Token.builder()
                .id(UUID.randomUUID().toString())
                .user(user)
                .token(jwtToken)
                .tokenType(TokenType.BEARER)
                .expired(false)
                .revoked(false)
                .build();
        user.setTokens(List.of(token));
        userRepository.save(user);
    }

    private void revokeAllUserTokens(User user) {
        List<Token> validUserTokens = tokenRepository.findAllValidTokenByUser(user.getId());
        if (validUserTokens.isEmpty())
            return;
        validUserTokens.forEach(token -> {
            token.setExpired(true);
            token.setRevoked(true);
        });
        tokenRepository.saveAll(validUserTokens);
    }

}
