package com.nisum.usuarios.services;

import com.nisum.usuarios.dto.AutheticationRequest;
import com.nisum.usuarios.dto.CreateUserResponse;
import com.nisum.usuarios.dto.UserDTO;
import com.nisum.usuarios.dto.UserRegisterRequest;

import java.util.List;

public interface IUserService {

    public CreateUserResponse register(UserRegisterRequest userRegisterRequest);

    List<UserDTO> getAllUsers();

    void logout(UserDTO userDTO);
}
