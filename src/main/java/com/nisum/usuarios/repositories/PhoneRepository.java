package com.nisum.usuarios.repositories;

import com.nisum.usuarios.model.Phone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;
@Repository
public interface PhoneRepository extends JpaRepository<Phone, UUID> {
}
