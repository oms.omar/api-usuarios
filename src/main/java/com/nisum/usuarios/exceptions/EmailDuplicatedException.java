package com.nisum.usuarios.exceptions;

public class EmailDuplicatedException extends RuntimeException {

    private int code;

    public EmailDuplicatedException(String message, int code) {
        super(message);
        this.code = code;
    }
}
