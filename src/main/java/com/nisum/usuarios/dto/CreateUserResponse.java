package com.nisum.usuarios.dto;

import lombok.*;

import java.time.LocalDateTime;
import java.util.UUID;
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreateUserResponse {

    private UUID id;
    private LocalDateTime created;
    private LocalDateTime modified;

    private LocalDateTime lastLogin;
    private String token;
    private boolean isActive;
}
