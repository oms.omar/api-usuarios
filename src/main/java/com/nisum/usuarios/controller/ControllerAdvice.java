package com.nisum.usuarios.controller;

import com.nisum.usuarios.exceptions.ApiErrorDTO;
import com.nisum.usuarios.exceptions.EmailDuplicatedException;
import com.nisum.usuarios.exceptions.InvalidPasswordException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ControllerAdvice {

    @ExceptionHandler(value = EmailDuplicatedException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<ApiErrorDTO> conflictExceptionHandler(EmailDuplicatedException ex) {
        return new ResponseEntity<ApiErrorDTO>(ApiErrorDTO.builder().mensaje(List.of(ex.getMessage())).build(), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ApiErrorDTO> notValidException(MethodArgumentNotValidException ex) {
        List<FieldError> errors = ex.getFieldErrors();
        ApiErrorDTO apiErrorDTO = ApiErrorDTO
                .builder()
                .mensaje(errors.stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.toList()))
                .build();
        return new ResponseEntity<ApiErrorDTO>(ApiErrorDTO.builder().mensaje(apiErrorDTO.getMensaje()).build(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = InvalidPasswordException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<ApiErrorDTO> passwordExceptionHandler(InvalidPasswordException ex) {
        return new ResponseEntity<ApiErrorDTO>(ApiErrorDTO.builder().mensaje(List.of(ex.getMessage())).build(), HttpStatus.CONFLICT);
    }
}
