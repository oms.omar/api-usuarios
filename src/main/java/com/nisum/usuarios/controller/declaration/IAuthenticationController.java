package com.nisum.usuarios.controller.declaration;

import com.nisum.usuarios.dto.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@RequestMapping("/api/v1/auth")
public interface IAuthenticationController {

    @PostMapping("/register")
    public ResponseEntity<CreateUserResponse> register(@Valid @RequestBody UserRegisterRequest request);

    @PostMapping("/authenticate")
    public ResponseEntity<AuthenticationResponse> authenticate(@Valid @RequestBody AutheticationRequest request);

    @PostMapping("/logout")
    public ResponseEntity<UserDTO> logout(UserDTO userDTO);
}
