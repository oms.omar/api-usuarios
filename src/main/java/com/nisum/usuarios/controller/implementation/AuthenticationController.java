package com.nisum.usuarios.controller.implementation;

import com.nisum.usuarios.controller.declaration.IAuthenticationController;
import com.nisum.usuarios.dto.*;
import com.nisum.usuarios.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class AuthenticationController implements IAuthenticationController {

    private final UserService userService;

    public ResponseEntity<CreateUserResponse> register(@Valid @RequestBody UserRegisterRequest request) {
        return ResponseEntity.ok(userService.register(request));
    }

    public ResponseEntity<AuthenticationResponse> authenticate(@Valid @RequestBody AutheticationRequest request) {
        return ResponseEntity.ok(userService.authenticate(request));
    }

    @Override
    public ResponseEntity<UserDTO> logout(@RequestBody UserDTO userDTO) {
        userService.logout(userDTO);
        return ResponseEntity.ok().build();
    }
}
