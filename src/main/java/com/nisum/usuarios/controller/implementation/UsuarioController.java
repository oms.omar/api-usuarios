package com.nisum.usuarios.controller.implementation;

import com.nisum.usuarios.controller.declaration.IUsuarioController;
import com.nisum.usuarios.dto.UserDTO;
import com.nisum.usuarios.services.IUserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UsuarioController implements IUsuarioController {

    private final IUserService userService;

    public UsuarioController(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public ResponseEntity<List<UserDTO>> getAllUsers() {
        return ResponseEntity.ok(userService.getAllUsers());
    }
}
