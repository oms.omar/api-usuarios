package com.nisum.usuarios.services;

import com.nisum.usuarios.datamock.UserRegisterRequestMock;
import com.nisum.usuarios.dto.CreateUserResponse;
import com.nisum.usuarios.dto.UserRegisterRequest;
import com.nisum.usuarios.exceptions.EmailDuplicatedException;
import com.nisum.usuarios.exceptions.InvalidPasswordException;
import com.nisum.usuarios.model.User;
import com.nisum.usuarios.repositories.TokenRepository;
import com.nisum.usuarios.repositories.UserRepository;
import com.nisum.usuarios.security.JwtService;
import com.nisum.usuarios.util.PasswordValidation;
import org.apache.logging.log4j.util.Strings;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    private UserRegisterRequest registerRequestMock;
    private User userMock;
    private CreateUserResponse createUserResponseMock;
    private Validator validator;
    @Mock
    private UserRepository userRepository;
    @Mock
    private JwtService jwtService;
    @Mock
    private AuthenticationManager authenticationManager;
    @Mock
    private PasswordValidation passwordValidation;
    @Mock
    private UserMapper userMapper;
    @Mock
    private TokenRepository tokenRepository;
    @InjectMocks
    private UserService userService;

    @BeforeEach
    void init(){
        CreateUserResponse createUserResponseExpected =
                CreateUserResponse
                        .builder()
                        .lastLogin(UserRegisterRequestMock.time())
                        .created(UserRegisterRequestMock.time())
                        .isActive(Boolean.TRUE)
                        .id(UserRegisterRequestMock.uuidStaticMock())
                        .token(UserRegisterRequestMock.tokenMock())
                        .build();
        this.registerRequestMock = UserRegisterRequestMock.userRegisterRequestMock();
        this.userMock = UserRegisterRequestMock.userMock();
        this.createUserResponseMock = UserRegisterRequestMock.createUserResponseMock();
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    @Test
    void whenAllValidationAreOk_thenUserRegister() {
        CreateUserResponse createUserResponseExpected =
                CreateUserResponse
                        .builder()
                        .lastLogin(UserRegisterRequestMock.time())
                        .created(UserRegisterRequestMock.time())
                        .isActive(Boolean.TRUE)
                        .id(UserRegisterRequestMock.uuidStaticMock())
                        .token(UserRegisterRequestMock.tokenMock())
                        .build();

        when(userRepository.findByEmail(any())).thenReturn(Optional.empty());
        when(passwordValidation.isValidPassword(any())).thenReturn(true);
        when(userMapper.userDTOtoUser(any())).thenReturn(UserRegisterRequestMock.userMock());
        when(jwtService.generateToken(any())).thenReturn(UserRegisterRequestMock.uuidStaticMock().toString());
        when(userRepository.save(any())).thenReturn(userMock);
        when(userMapper.userToUserResponseDTO(any(), any())).thenReturn(createUserResponseMock);

        CreateUserResponse createUserResponse = userService.register(registerRequestMock);

        assertEquals(createUserResponse.getId(), createUserResponseExpected.getId());
    }

    @Test
    void whenEmailAlreadyExist_thenUserCannotBeRegistered() {
        String emailExpected = "jhondoe@jhondoe.com";
        when(userRepository.findByEmail(any())).thenThrow(EmailDuplicatedException.class);
        EmailDuplicatedException emailDuplicatedException = assertThrows(EmailDuplicatedException.class, () -> {
            userService.register(registerRequestMock);
        });
       assertEquals(EmailDuplicatedException.class, emailDuplicatedException.getClass());
    }

    @Test
    void whenEmailStructureIsInvalid_thenValidationError() {
        UserRegisterRequest userRegisterRequest =
                UserRegisterRequestMock.userRegisterRequestMock();
        userRegisterRequest.setEmail(UserRegisterRequestMock.BAD_EMAIL_STRUCTURE);

        Set<ConstraintViolation<UserRegisterRequest>> violation = validator.validate(userRegisterRequest);
        assertFalse(violation.isEmpty());
        assertTrue(violation.stream().anyMatch(a -> a.getMessage().contains("El formato del email no es correcto.")));
    }

    @Test
    public void whenAllFieldsAreValid_thenValidationSucceeds() {
        UserRegisterRequest usuarioDTO = new UserRegisterRequest();
        usuarioDTO.setName("Juan");
        usuarioDTO.setPassword("Shar$123");
        usuarioDTO.setEmail("juan.perez@example.com");
        usuarioDTO.setPhones(UserRegisterRequestMock.phonesMock());

        Set<ConstraintViolation<UserRegisterRequest>> violations = validator.validate(usuarioDTO);

        assertTrue(violations.isEmpty());
    }

    @Test
    void notUserRegisterWhenNameIsBlank() {
        UserRegisterRequest userRegisterRequest =
                UserRegisterRequestMock.userRegisterRequestMock();
        userRegisterRequest.setName(Strings.EMPTY);

        Set<ConstraintViolation<UserRegisterRequest>> violation = validator.validate(userRegisterRequest);
        assertFalse(violation.isEmpty());
        assertTrue(violation.stream().anyMatch(a -> a.getMessage().contains("El campo name no puede ser nulo")));
    }

    @Test
    void notUserRegisterWhenPasswordIsBlank() {
        UserRegisterRequest userRegisterRequest =
                UserRegisterRequestMock.userRegisterRequestMock();
        userRegisterRequest.setPassword(Strings.EMPTY);

        Set<ConstraintViolation<UserRegisterRequest>> violation = validator.validate(userRegisterRequest);
        assertFalse(violation.isEmpty());
        assertTrue(violation.stream().anyMatch(a -> a.getMessage().contains("El password no puede ser nulo")));
    }

    @Test
    void userRegisterWhenPasswordHasInvalidStructure() {
        when(passwordValidation.isValidPassword(any())).thenThrow(InvalidPasswordException.class);

        InvalidPasswordException invalidPasswordException = assertThrows(InvalidPasswordException.class, () -> {
            userService.register(registerRequestMock);
        });
        assertEquals(InvalidPasswordException.class, invalidPasswordException.getClass());

    }

}
