FROM openjdk:11
EXPOSE 8090
ADD target/api-usuarios.jar api-usuarios.jar
ENTRYPOINT ["java", "-jar", "/api-usuarios.jar"]